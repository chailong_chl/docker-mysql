FROM alex/ubuntu:14.04

MAINTAINER Alex Zhang <zhangdi_me@163.com>

ENV DATA_DIR /var/lib/mysql

# Install MySQL
RUN apt-get install -y mysql-client mysql-server

RUN apt-get autoclean

ADD mysqld_charset.cnf /etc/my.cnf.d/

COPY scripts /scripts
RUN chmod +x /scripts/run-mysql.sh
RUN chmod +x /scripts/firstrun_mysql.sh

EXPOSE 3306

VOLUME ["/var/lib/mysql"]

ENTRYPOINT ["/scripts/run-mysql.sh"]
